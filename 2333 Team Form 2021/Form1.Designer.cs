﻿
namespace _2333_Team_Form_2021
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.Label teamnumberLabel;
            System.Windows.Forms.Label drivesystemLabel;
            System.Windows.Forms.Label scorefuelLabel;
            System.Windows.Forms.Label goalLabel;
            System.Windows.Forms.Label gearsLabel;
            System.Windows.Forms.Label climbLabel;
            System.Windows.Forms.Label autoLabel;
            this.team2333DataSet = new _2333_Team_Form_2021.team2333DataSet();
            this.teamdataBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.teamdataTableAdapter = new _2333_Team_Form_2021.team2333DataSetTableAdapters.teamdataTableAdapter();
            this.tableAdapterManager = new _2333_Team_Form_2021.team2333DataSetTableAdapters.TableAdapterManager();
            this.teamdataBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.teamdataBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.teamnumberTextBox = new System.Windows.Forms.TextBox();
            this.drivesystemTextBox = new System.Windows.Forms.TextBox();
            this.scorefuelCheckBox = new System.Windows.Forms.CheckBox();
            this.goalTextBox = new System.Windows.Forms.TextBox();
            this.gearsCheckBox = new System.Windows.Forms.CheckBox();
            this.climbCheckBox = new System.Windows.Forms.CheckBox();
            this.autoCheckBox = new System.Windows.Forms.CheckBox();
            this.teamdataDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.setup_bttn = new System.Windows.Forms.Button();
            this.sync_bttn = new System.Windows.Forms.Button();
            teamnumberLabel = new System.Windows.Forms.Label();
            drivesystemLabel = new System.Windows.Forms.Label();
            scorefuelLabel = new System.Windows.Forms.Label();
            goalLabel = new System.Windows.Forms.Label();
            gearsLabel = new System.Windows.Forms.Label();
            climbLabel = new System.Windows.Forms.Label();
            autoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.team2333DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamdataBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamdataBindingNavigator)).BeginInit();
            this.teamdataBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamdataDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // team2333DataSet
            // 
            this.team2333DataSet.DataSetName = "team2333DataSet";
            this.team2333DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // teamdataBindingSource
            // 
            this.teamdataBindingSource.DataMember = "teamdata";
            this.teamdataBindingSource.DataSource = this.team2333DataSet;
            // 
            // teamdataTableAdapter
            // 
            this.teamdataTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.teamdataTableAdapter = this.teamdataTableAdapter;
            this.tableAdapterManager.UpdateOrder = _2333_Team_Form_2021.team2333DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // teamdataBindingNavigator
            // 
            this.teamdataBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.teamdataBindingNavigator.BindingSource = this.teamdataBindingSource;
            this.teamdataBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.teamdataBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.teamdataBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.teamdataBindingNavigatorSaveItem});
            this.teamdataBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.teamdataBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.teamdataBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.teamdataBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.teamdataBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.teamdataBindingNavigator.Name = "teamdataBindingNavigator";
            this.teamdataBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.teamdataBindingNavigator.Size = new System.Drawing.Size(1036, 25);
            this.teamdataBindingNavigator.TabIndex = 0;
            this.teamdataBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // teamdataBindingNavigatorSaveItem
            // 
            this.teamdataBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.teamdataBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("teamdataBindingNavigatorSaveItem.Image")));
            this.teamdataBindingNavigatorSaveItem.Name = "teamdataBindingNavigatorSaveItem";
            this.teamdataBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.teamdataBindingNavigatorSaveItem.Text = "Save Data";
            this.teamdataBindingNavigatorSaveItem.Click += new System.EventHandler(this.teamdataBindingNavigatorSaveItem_Click);
            // 
            // teamnumberLabel
            // 
            teamnumberLabel.AutoSize = true;
            teamnumberLabel.Location = new System.Drawing.Point(18, 54);
            teamnumberLabel.Name = "teamnumberLabel";
            teamnumberLabel.Size = new System.Drawing.Size(68, 13);
            teamnumberLabel.TabIndex = 1;
            teamnumberLabel.Text = "teamnumber:";
            // 
            // teamnumberTextBox
            // 
            this.teamnumberTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.teamdataBindingSource, "teamnumber", true));
            this.teamnumberTextBox.Location = new System.Drawing.Point(92, 51);
            this.teamnumberTextBox.Name = "teamnumberTextBox";
            this.teamnumberTextBox.Size = new System.Drawing.Size(104, 20);
            this.teamnumberTextBox.TabIndex = 2;
            // 
            // drivesystemLabel
            // 
            drivesystemLabel.AutoSize = true;
            drivesystemLabel.Location = new System.Drawing.Point(18, 80);
            drivesystemLabel.Name = "drivesystemLabel";
            drivesystemLabel.Size = new System.Drawing.Size(65, 13);
            drivesystemLabel.TabIndex = 3;
            drivesystemLabel.Text = "drivesystem:";
            // 
            // drivesystemTextBox
            // 
            this.drivesystemTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.teamdataBindingSource, "drivesystem", true));
            this.drivesystemTextBox.Location = new System.Drawing.Point(92, 77);
            this.drivesystemTextBox.Name = "drivesystemTextBox";
            this.drivesystemTextBox.Size = new System.Drawing.Size(104, 20);
            this.drivesystemTextBox.TabIndex = 4;
            // 
            // scorefuelLabel
            // 
            scorefuelLabel.AutoSize = true;
            scorefuelLabel.Location = new System.Drawing.Point(18, 108);
            scorefuelLabel.Name = "scorefuelLabel";
            scorefuelLabel.Size = new System.Drawing.Size(53, 13);
            scorefuelLabel.TabIndex = 5;
            scorefuelLabel.Text = "scorefuel:";
            // 
            // scorefuelCheckBox
            // 
            this.scorefuelCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.teamdataBindingSource, "scorefuel", true));
            this.scorefuelCheckBox.Location = new System.Drawing.Point(92, 103);
            this.scorefuelCheckBox.Name = "scorefuelCheckBox";
            this.scorefuelCheckBox.Size = new System.Drawing.Size(104, 24);
            this.scorefuelCheckBox.TabIndex = 6;
            this.scorefuelCheckBox.Text = "checkBox1";
            this.scorefuelCheckBox.UseVisualStyleBackColor = true;
            // 
            // goalLabel
            // 
            goalLabel.AutoSize = true;
            goalLabel.Location = new System.Drawing.Point(18, 136);
            goalLabel.Name = "goalLabel";
            goalLabel.Size = new System.Drawing.Size(30, 13);
            goalLabel.TabIndex = 7;
            goalLabel.Text = "goal:";
            // 
            // goalTextBox
            // 
            this.goalTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.teamdataBindingSource, "goal", true));
            this.goalTextBox.Location = new System.Drawing.Point(92, 133);
            this.goalTextBox.Name = "goalTextBox";
            this.goalTextBox.Size = new System.Drawing.Size(104, 20);
            this.goalTextBox.TabIndex = 8;
            // 
            // gearsLabel
            // 
            gearsLabel.AutoSize = true;
            gearsLabel.Location = new System.Drawing.Point(18, 164);
            gearsLabel.Name = "gearsLabel";
            gearsLabel.Size = new System.Drawing.Size(36, 13);
            gearsLabel.TabIndex = 9;
            gearsLabel.Text = "gears:";
            // 
            // gearsCheckBox
            // 
            this.gearsCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.teamdataBindingSource, "gears", true));
            this.gearsCheckBox.Location = new System.Drawing.Point(92, 159);
            this.gearsCheckBox.Name = "gearsCheckBox";
            this.gearsCheckBox.Size = new System.Drawing.Size(104, 24);
            this.gearsCheckBox.TabIndex = 10;
            this.gearsCheckBox.Text = "checkBox1";
            this.gearsCheckBox.UseVisualStyleBackColor = true;
            // 
            // climbLabel
            // 
            climbLabel.AutoSize = true;
            climbLabel.Location = new System.Drawing.Point(18, 194);
            climbLabel.Name = "climbLabel";
            climbLabel.Size = new System.Drawing.Size(34, 13);
            climbLabel.TabIndex = 11;
            climbLabel.Text = "climb:";
            // 
            // climbCheckBox
            // 
            this.climbCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.teamdataBindingSource, "climb", true));
            this.climbCheckBox.Location = new System.Drawing.Point(92, 189);
            this.climbCheckBox.Name = "climbCheckBox";
            this.climbCheckBox.Size = new System.Drawing.Size(104, 24);
            this.climbCheckBox.TabIndex = 12;
            this.climbCheckBox.Text = "checkBox1";
            this.climbCheckBox.UseVisualStyleBackColor = true;
            // 
            // autoLabel
            // 
            autoLabel.AutoSize = true;
            autoLabel.Location = new System.Drawing.Point(18, 224);
            autoLabel.Name = "autoLabel";
            autoLabel.Size = new System.Drawing.Size(31, 13);
            autoLabel.TabIndex = 13;
            autoLabel.Text = "auto:";
            // 
            // autoCheckBox
            // 
            this.autoCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.teamdataBindingSource, "auto", true));
            this.autoCheckBox.Location = new System.Drawing.Point(92, 219);
            this.autoCheckBox.Name = "autoCheckBox";
            this.autoCheckBox.Size = new System.Drawing.Size(104, 24);
            this.autoCheckBox.TabIndex = 14;
            this.autoCheckBox.Text = "checkBox1";
            this.autoCheckBox.UseVisualStyleBackColor = true;
            // 
            // teamdataDataGridView
            // 
            this.teamdataDataGridView.AutoGenerateColumns = false;
            this.teamdataDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.teamdataDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewCheckBoxColumn1,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewCheckBoxColumn2,
            this.dataGridViewCheckBoxColumn3,
            this.dataGridViewCheckBoxColumn4});
            this.teamdataDataGridView.DataSource = this.teamdataBindingSource;
            this.teamdataDataGridView.Location = new System.Drawing.Point(244, 51);
            this.teamdataDataGridView.Name = "teamdataDataGridView";
            this.teamdataDataGridView.Size = new System.Drawing.Size(750, 239);
            this.teamdataDataGridView.TabIndex = 15;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "teamnumber";
            this.dataGridViewTextBoxColumn1.HeaderText = "teamnumber";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "drivesystem";
            this.dataGridViewTextBoxColumn2.HeaderText = "drivesystem";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.DataPropertyName = "scorefuel";
            this.dataGridViewCheckBoxColumn1.HeaderText = "scorefuel";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "goal";
            this.dataGridViewTextBoxColumn3.HeaderText = "goal";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.DataPropertyName = "gears";
            this.dataGridViewCheckBoxColumn2.HeaderText = "gears";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.DataPropertyName = "climb";
            this.dataGridViewCheckBoxColumn3.HeaderText = "climb";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.DataPropertyName = "auto";
            this.dataGridViewCheckBoxColumn4.HeaderText = "auto";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            // 
            // setup_bttn
            // 
            this.setup_bttn.Location = new System.Drawing.Point(7, 267);
            this.setup_bttn.Name = "setup_bttn";
            this.setup_bttn.Size = new System.Drawing.Size(75, 23);
            this.setup_bttn.TabIndex = 16;
            this.setup_bttn.Text = "Setup";
            this.setup_bttn.UseVisualStyleBackColor = true;
            this.setup_bttn.Click += new System.EventHandler(this.setup_bttn_Click);
            // 
            // sync_bttn
            // 
            this.sync_bttn.Location = new System.Drawing.Point(121, 267);
            this.sync_bttn.Name = "sync_bttn";
            this.sync_bttn.Size = new System.Drawing.Size(75, 23);
            this.sync_bttn.TabIndex = 17;
            this.sync_bttn.Text = "Sync";
            this.sync_bttn.UseVisualStyleBackColor = true;
            this.sync_bttn.Click += new System.EventHandler(this.sync_bttn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1036, 318);
            this.Controls.Add(this.sync_bttn);
            this.Controls.Add(this.setup_bttn);
            this.Controls.Add(this.teamdataDataGridView);
            this.Controls.Add(teamnumberLabel);
            this.Controls.Add(this.teamnumberTextBox);
            this.Controls.Add(drivesystemLabel);
            this.Controls.Add(this.drivesystemTextBox);
            this.Controls.Add(scorefuelLabel);
            this.Controls.Add(this.scorefuelCheckBox);
            this.Controls.Add(goalLabel);
            this.Controls.Add(this.goalTextBox);
            this.Controls.Add(gearsLabel);
            this.Controls.Add(this.gearsCheckBox);
            this.Controls.Add(climbLabel);
            this.Controls.Add(this.climbCheckBox);
            this.Controls.Add(autoLabel);
            this.Controls.Add(this.autoCheckBox);
            this.Controls.Add(this.teamdataBindingNavigator);
            this.Name = "Form1";
            this.Text = "2333 Team Form";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.team2333DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamdataBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamdataBindingNavigator)).EndInit();
            this.teamdataBindingNavigator.ResumeLayout(false);
            this.teamdataBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamdataDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private team2333DataSet team2333DataSet;
        private System.Windows.Forms.BindingSource teamdataBindingSource;
        private team2333DataSetTableAdapters.teamdataTableAdapter teamdataTableAdapter;
        private team2333DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator teamdataBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton teamdataBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox teamnumberTextBox;
        private System.Windows.Forms.TextBox drivesystemTextBox;
        private System.Windows.Forms.CheckBox scorefuelCheckBox;
        private System.Windows.Forms.TextBox goalTextBox;
        private System.Windows.Forms.CheckBox gearsCheckBox;
        private System.Windows.Forms.CheckBox climbCheckBox;
        private System.Windows.Forms.CheckBox autoCheckBox;
        private System.Windows.Forms.DataGridView teamdataDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.Button setup_bttn;
        private System.Windows.Forms.Button sync_bttn;
    }
}

