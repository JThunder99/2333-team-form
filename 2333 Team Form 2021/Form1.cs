﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Synchronization.Data.SqlServer;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization;

namespace _2333_Team_Form_2021
{
    public partial class Form1 : Form
    {
        public static string sqlremoteConnectionString = "Data Source=0.0.0.0,1433;Network Library=DBMSSOCN;Initial Catalog=team2333;User ID=username;Password=password";
        public static string sqllocalConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\team2333.mdf;Integrated Security=True";
        public static readonly string scopeName = "alltablesyncgroup";
        public string logs = "";

        public Form1()
        {
            InitializeComponent();
        }

        public static void Setup()
        {
            try
            {
                SqlConnection sqlLocalConn = new SqlConnection(sqllocalConnectionString);
                SqlConnection sqlServerConn = new SqlConnection(sqlremoteConnectionString);
                DbSyncScopeDescription myScope = new DbSyncScopeDescription(scopeName);

                DbSyncTableDescription TeamData = SqlSyncDescriptionBuilder.GetDescriptionForTable("teamdata", sqlServerConn);

                // Add the table from above to the scope
                myScope.Tables.Add(TeamData);

                // Setup SQL Local for sync
                SqlSyncScopeProvisioning sqlLocalProv = new SqlSyncScopeProvisioning(sqlLocalConn, myScope);
                if (!sqlLocalProv.ScopeExists(scopeName))
                {
                    // Apply the scope provisioning.
                    MessageBox.Show("Provisioning SQL Local Server for sync " + DateTime.Now);
                    sqlLocalProv.Apply();
                    MessageBox.Show("Done Provisioning SQL Local Server for sync " + DateTime.Now);
                }
                else
                    MessageBox.Show("SQL Local Datebase server already provisioned for sync " + DateTime.Now);

                //Setup SQL Server for sync
                SqlSyncScopeProvisioning sqlServerProv = new SqlSyncScopeProvisioning(sqlServerConn, myScope);
                if (!sqlServerProv.ScopeExists(scopeName))
                {
                    // Apply the scope provisioning.
                    MessageBox.Show("Provisioning SQL Remote Server for sync " + DateTime.Now);
                    sqlServerProv.Apply();
                    MessageBox.Show("Done Provisioning SQL Remote Server for sync " + DateTime.Now);
                }
                else
                    MessageBox.Show("SQL Remote Datebase server already provisioned for sync " + DateTime.Now);

                sqlServerConn.Close();
                sqlLocalConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public static void Sync()
        {
            try
            {
                SqlConnection sqlLocalConn = new SqlConnection(sqllocalConnectionString);
                SqlConnection sqlServerConn = new SqlConnection(sqlremoteConnectionString);
                SyncOrchestrator orch = new SyncOrchestrator
                {
                    RemoteProvider = new SqlSyncProvider(scopeName, sqlServerConn),
                    LocalProvider = new SqlSyncProvider(scopeName, sqlLocalConn),
                    Direction = SyncDirectionOrder.UploadAndDownload
                };
                Console.WriteLine("ScopeName={0} ", scopeName.ToUpper());
                Console.WriteLine("Starting Sync " + DateTime.Now);
                ShowStatistics(orch.Synchronize());

                sqlServerConn.Close();
                sqlLocalConn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }

        public static void ShowStatistics(SyncOperationStatistics syncStats)
        {
            string message;
            string message2;
            string message3;
            string message4;
            string message5;
            string message6;
            string message7;
            string message8;

            message = "\tSync Start Time :" + syncStats.SyncStartTime.ToString();
            Console.WriteLine(message);
            message2 = "\tSync End Time :" + syncStats.SyncEndTime.ToString();
            Console.WriteLine(message);
            message3 = "\tUpload Changes Applied :" + syncStats.UploadChangesApplied.ToString();
            Console.WriteLine(message);
            message4 = "\tUpload Changes Failed :" + syncStats.UploadChangesFailed.ToString();
            Console.WriteLine(message);
            message5 = "\tUpload Changes Total :" + syncStats.UploadChangesTotal.ToString();
            Console.WriteLine(message);
            message6 = "\tDownload Changes Applied :" + syncStats.DownloadChangesApplied.ToString();
            Console.WriteLine(message);
            message7 = "\tDownload Changes Failed :" + syncStats.DownloadChangesFailed.ToString();
            Console.WriteLine(message);
            message8 = "\tDownload Changes Total :" + syncStats.DownloadChangesTotal.ToString();
            Console.WriteLine(message);

            MessageBox.Show(message + "\n" + message2 + "\n" + message3 + "\n" + message4 + "\n" + message5 + "\n" + message6 + "\n" + message7 + "\n" + message8);
        }

        private void teamdataBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.teamdataBindingSource.EndEdit();
                this.tableAdapterManager.UpdateAll(this.team2333DataSet);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'team2333DataSet.teamdata' table. You can move, or remove it, as needed.
            this.teamdataTableAdapter.Fill(this.team2333DataSet.teamdata);

        }

        private void setup_bttn_Click(object sender, EventArgs e)
        {
            Setup();
        }

        private void sync_bttn_Click(object sender, EventArgs e)
        {
            Sync();
        }
    }
}
